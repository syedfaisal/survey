import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:survey4good/Screens/BrandHomeScreen.dart';
import 'package:survey4good/Utils/Colors.dart';

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey();
  GlobalKey globalKey = new GlobalKey(debugLabel: 'btm_app_bar');
  int _selectedIndex = 0;

  @override
  void initState() {
    super.initState();
    setState(() {
      _selectedIndex = 0;
    });
  }

  @override
  Widget build(BuildContext context) {
    List<Widget> ScreensForHome = [
      BrandHomeScreen(),
      BrandHomeScreen(),
      BrandHomeScreen(),
      BrandHomeScreen(),
      BrandHomeScreen(),
    ];

    return Scaffold(
      // key: _scaffoldKey,
      body: ScreensForHome[_selectedIndex],
      bottomNavigationBar: BottomNavigationBar(
        // key: globalKey,
        currentIndex: _selectedIndex,
        backgroundColor: Colors.white,
        selectedItemColor: baseColor,
        unselectedItemColor: Colors.grey,
        showSelectedLabels: false,
        showUnselectedLabels: false,
        type: BottomNavigationBarType.fixed,
        items: [
          BottomNavigationBarItem(
            icon: SvgPicture.asset(
              'images/Icon_metro_home.svg',
              height: 20,
              color: _selectedIndex == 0 ? baseColor : Colors.grey,
            ),
            label: '',
          ),
          BottomNavigationBarItem(
            icon: SvgPicture.asset(
              'images/heart_menu.svg',
              height: 25,
              color:  Colors.grey,
              // color: _selectedIndex == 1 ? baseColor : Colors.grey,
            ),
            label: '',
//             title: Container(
//               child: Column(
//                 children: <Widget>[
//                   Text(
//                     tr('homeScreen.bottomNavigationCommunityTitle'),
// //                AppLocalizations.of(context)
// //                    .tr('HomeScreen.homeScreenBottomBar.communityTitle'),
//                     style: TextStyle(fontSize: 11),
//                   ),
//                   SizedBox(
//                     height: 10,
//                   ),
//                   (_selectedIndex == 1
//                       ? SvgPicture.asset(
//                           'images/arrow_indicator.svg',
//                           height: 6,
//                           color: whiteColor,
//                         )
//                       : Container(
//                           height: 7,
//                         ))
//                 ],
//               ),
//             ),
          ),
          BottomNavigationBarItem(
            icon: SvgPicture.asset(
              'images/menu_shield.svg',
              height: 25,
              color:  Colors.grey,
            ),
            label: '',
          ),
          BottomNavigationBarItem(
            icon: SvgPicture.asset(
              'images/person_menu.svg',
              height: 25,
              color:  Colors.grey,
              // color: _selectedIndex == 3 ? baseColor : Colors.grey,
            ),
            label: '',
          ),
          BottomNavigationBarItem(
            icon: Padding(
              padding: EdgeInsets.all(10.0),
              child: SvgPicture.asset(
                'images/share_menu.svg',
                height: 25,
                color:  Colors.grey,
                // color: _selectedIndex == 4 ? baseColor : Colors.grey,
              ),
            ),
            label: '',
          ),
        ],
        onTap: (index) {
          setState(() {
            _selectedIndex = index;
          });
        },
      ),
    );
  }
}
