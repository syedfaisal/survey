import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:survey4good/Data/BrandsList.dart';
import 'package:survey4good/Models/Brands.dart';
import 'package:survey4good/Utils/Colors.dart';

class DetailQuestionScreen extends StatefulWidget {
  final Brands brand;

  const DetailQuestionScreen({@required this.brand});

  @override
  _DetailQuestionScreenState createState() => _DetailQuestionScreenState();
}

class _DetailQuestionScreenState extends State<DetailQuestionScreen> {
  var _brandList = brandsList;

  @override
  Widget build(BuildContext context) {
    return Container(
        decoration: BoxDecoration(
          image: DecorationImage(
            image: AssetImage("images/background.png"),
            fit: BoxFit.cover,
          ),
        ),
        child: Scaffold(
          backgroundColor: Colors.transparent,
          body: Column(
            children: [
              SizedBox(
                height: 18,
              ),
              Container(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    GestureDetector(
                      onTap: () {
                        Navigator.pop(context);
                      },
                      child: Container(
                        padding: EdgeInsets.only(
                            left: 15, right: 2, top: 5, bottom: 5),
                        child: Image.asset(
                          'images/arrow_back.png',
                          width: 23,
                          height: 20,
                        ),
                      ),
                    ),
                    Container(
                      padding: EdgeInsets.only(
                          left: 15, right: 2, top: 5, bottom: 5),
                      child: Image.asset(
                        'images/logo.png',
                        width: 70,
                        height: 70,
                        color: baseColor,
                      ),
                    ),
                    SizedBox(
                      width: 65,
                    )
                  ],
                ),
              ),
              SizedBox(
                height: 20,
              ),
              Wrap(
                children: [
                  Padding(
                    padding: EdgeInsets.only(left: 45, right: 45),
                    child: Text(
                      widget.brand.questions,
                      textAlign: TextAlign.center,
                      style: TextStyle(
                          color: darkColor,
                          fontFamily: "RobotoCondensed",
                          fontWeight: FontWeight.w900,
                          fontSize: 17),
                    ),
                  ),
                ],
              ),
              SizedBox(
                height: 10,
              ),
              Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Container(
                    height: 25,
                    width: 25,
                    margin: EdgeInsets.only(left: 2, right: 5),
                    child: Center(
                      child: Image.asset(
                        widget.brand.image,
                        fit: BoxFit.cover,
                      ),
                    ),
                  ),
                  Text(
                    widget.brand.name,
                    textAlign: TextAlign.center,
                    style: TextStyle(
                        color: baseColor,
                        fontWeight: FontWeight.w500,
                        fontSize: 16),
                  ),
                ],
              ),
              Center(
                child: Container(
                  margin: EdgeInsets.only(left: 40, right: 40, top: 10),
                  height: MediaQuery.of(context).size.height * .62,
                  decoration: BoxDecoration(
                    image: DecorationImage(
                      image: AssetImage("images/img.png"),
                      fit: BoxFit.fill,
                    ),
                    borderRadius: BorderRadius.circular(25),
                  ),
                ),
              ),
              FlatButton(
                height: 40,
                child: Text(
                  'Begin Survey',
                  style: TextStyle(fontSize: 14.0, fontWeight: FontWeight.w400),
                ),
                color: baseColor,
                textColor: Colors.white,
                onPressed: () {},
              ),
            ],
          ),
        ));
  }

  Widget brandsWidget(Brands brand) {
    return Container(
      height: 200,
      width: 200,
      child: Padding(
        padding: const EdgeInsets.only(bottom: 18.0),
        child: Stack(
          children: [
            Align(
              alignment: Alignment.bottomCenter,
              child:
                  // Card(
                  //   shadowColor: Colors.black,
                  //   color: Colors.white.withOpacity(0.71),
                  //   elevation: 5,
                  //   shape: RoundedRectangleBorder(
                  //     side: BorderSide(
                  //         color: Colors.white.withOpacity(0.71), width: 0),
                  //     borderRadius: BorderRadius.circular(15),
                  //   ),
                  //   child:
                  Container(
                height: 170,
                width: 155,
                decoration: BoxDecoration(
                  boxShadow: [
                    BoxShadow(
                      color: Colors.black12,
                      blurRadius: 11.0,
                      // has the effect of softening the shadow
                      spreadRadius: 1,
                      // has the effect of extending the
                      offset: Offset(0, 6), // shadow
                    )
                  ],
                  borderRadius: BorderRadius.circular(15),
                  color: Colors.grey[200].withOpacity(0.98),
                ),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    SizedBox(
                      height: 60,
                    ),
                    Text(
                      brand.name,
                      textAlign: TextAlign.center,
                      style: TextStyle(
                          color: darkColor,
                          fontFamily: "RobotoCondensed",
                          fontWeight: FontWeight.w900,
                          fontSize: 16),
                    ),
                    Wrap(
                      children: [
                        Padding(
                          padding: EdgeInsets.only(left: 15, right: 15),
                          child: Text(
                            brand.questions,
                            textAlign: TextAlign.center,
                            style: TextStyle(
                                color: darkColor,
                                fontFamily: "RobotoCondensed",
                                fontWeight: FontWeight.w500,
                                fontSize: 10),
                          ),
                        )
                      ],
                    ),
                    Align(
                      alignment: Alignment.bottomCenter,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Text(
                            'Earn',
                            textAlign: TextAlign.left,
                            style: TextStyle(
                                color: baseColor,
                                fontWeight: FontWeight.w900,
                                fontSize: 12),
                          ),
                          SizedBox(
                            width: 10,
                          ),
                          Container(
                            width: 65,
                            height: 25,
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(15),
                              color: baseColor,
                            ),
                            child: Row(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                Container(
                                  height: 20,
                                  width: 20,
                                  margin: EdgeInsets.only(left: 2, right: 5),
                                  child: Center(
                                    child: Image.asset(
                                      'images/token.png',
                                      fit: BoxFit.cover,
                                    ),
                                  ),
                                  // decoration: BoxDecoration(
                                  //   boxShadow: [
                                  //     BoxShadow(
                                  //       color: Colors.black12,
                                  //       blurRadius: 1.0,
                                  //       // has the effect of softening the shadow
                                  //       spreadRadius: 1,
                                  //       // has the effect of extending the
                                  //       offset: Offset(6, 0), // shadow
                                  //     )
                                  //   ],
                                  //   borderRadius: BorderRadius.circular(15),
                                  //   color: Color(0xFFD8D8D8),
                                  // ),
                                ),
                                Text(
                                  brand.price,
                                  textAlign: TextAlign.left,
                                  style: TextStyle(
                                      color: Colors.white,
                                      fontWeight: FontWeight.w400,
                                      fontSize: 10),
                                ),
                              ],
                            ),
                          )
                        ],
                      ),
                    ),
                    SizedBox(
                      height: 5,
                    ),
                  ],
                ),
              ),
            ),
            Align(
              alignment: Alignment.topCenter,
              child: Card(
                shadowColor: Colors.black,
                color: Colors.white,
                elevation: 3,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(20),
                ),
                child: Container(
                  height: 80,
                  width: 80,
                  child: Center(
                    child: Image.asset(
                      brand.image,
                      width: 50,
                      height: 50,
                    ),
                  ),
                  // decoration: BoxDecoration(
                  //   boxShadow: [
                  //     BoxShadow(
                  //       color: Colors.black12,
                  //       blurRadius: 1.0,
                  //       // has the effect of softening the shadow
                  //       spreadRadius: 1,
                  //       // has the effect of extending the
                  //       offset: Offset(6, 0), // shadow
                  //     )
                  //   ],
                  //   borderRadius: BorderRadius.circular(15),
                  //   color: Color(0xFFD8D8D8),
                  // ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
