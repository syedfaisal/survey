import 'package:survey4good/Models/Brands.dart';

var brandsList = [
  Brands(
      name: "4Good Survey",
      image: 'images/icon_heart.png',
      price: '25,000',
      questions: 'Answer a 6 question survey and earn'),
  Brands(
      name: "P&G Survey",
      image: 'images/icon_pg.png',
      price: '25,000',
      questions: 'Answer a 6 question shopping habits'),
  Brands(
      name: "RBC",
      image: 'images/icon_rbc.png',
      price: '25,000',
      questions: '8 question survey about banking habits'),
  Brands(
      name: "King Games",
      image: 'images/icon_king.png',
      price: '25,000',
      questions:
          'Tell us about your gaming habits and earn tokens you can play with'),
  Brands(
      name: "4Good Survey",
      image: 'images/icon_star.png',
      price: '25,000',
      questions: 'Answer a 6 question survey '),
  Brands(
      name: "Toyota",
      image: 'images/icon_toyota.png',
      price: '25,000',
      questions: 'Answer a 6 question think in the car of the future'),
];
