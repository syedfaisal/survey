class Brands {
  String name, image, price, questions;

  Brands({
    this.name,
    this.image,
    this.price,
    this.questions,
  });
}
